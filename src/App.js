import { useState, useEffect } from "react";
import { createClient } from "@supabase/supabase-js";

import "./App.css";

const supabaseUrl = process.env.REACT_APP_SUPABASEURL;
const supabaseKey = process.env.REACT_APP_SUPABASEKEY;
const supabase = createClient(supabaseUrl, supabaseKey);

function Todo({ todo, index, completeTodo, deleteTodo }) {
  return (
    <div>
      <div className={`todo ${todo.isCompleted ? "complete" : ""}`}>
        {todo.name}
      </div>
      <button onClick={() => completeTodo(index, todo.id)}>Complete</button>
      <button onClick={() => deleteTodo(index, todo.id)}>x</button>
    </div>
  );
}

function AddTodo({ addTodo }) {
  const [newValue, setNewValue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    addTodo(newValue);
    setNewValue("");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          onChange={(e) => setNewValue(e.target.value)}
          value={newValue}
          placeholder="Add todo..."
        />
      </form>
    </div>
  );
}

function App() {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    getTodos();
  });

  const getTodos = async () => {
    try {
      let { data: todos } = await supabase.from("todos").select("*");

      setTodos(todos);
    } catch (error) {
      console.log("error", error);
    }
  };

  const completeTodo = async (index, id) => {
    if (window.confirm("Are you sure you want to mark complete?")) {
      await supabase.from("todos").update({ isCompleted: true }).eq("id", id);

      //let currentTodos = [...todos];
      //currentTodos[index].isCompleted = true;
      //setTodos(currentTodos);
    }
  };

  const addTodo = async (todo) => {
    await supabase.from("todos").insert([{ name: todo, isCompleted: false }]);

    //let newTodo = {name: todo, isCompleted:false};
    //let newTodos = [...todos,newTodo];
    //setTodos(newTodos);
  };

  const deleteTodo = async (index, id) => {
    if (window.confirm("Are you sure you want to delete?")) {
      await supabase.from("todos").delete().eq("id", id);

      //let newTodos = [...todos];
      //newTodos.splice(index,1);
      //setTodos(newTodos);
    }
  };

  return (
    <div>
      {todos.map((todo, index) => (
        <Todo
          todo={todo}
          key={index}
          index={index}
          completeTodo={completeTodo}
          deleteTodo={deleteTodo}
        />
      ))}
      <AddTodo addTodo={addTodo} />
    </div>
  );
}

export default App;
